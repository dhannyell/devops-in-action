# Automatizando a Infraestrutura para deploy da API para Validação de CPF

Nesse deploy o objetivo é automatizar a configuração e implantação da aplicação no servidor. Com isso a infraestrutura fica documentada, automatizada, diminui-se os riscos de erros, o processo fica escalável e mais rápido.

## Pré-requisitos

- Docker instalado
- Git instalado
- Ansible instalado

## Setup

Agora nosso servidor já está no ar, mas o processo ainda é em boa parte manual, vamos extrair os passos para um playbook usando o Ansible para automatizarmos o processo.

Baseado no processo feito no `deploy_1` vamos iniciar a configuração do playbook.

## Ansible Playbook

Para esse exercício vamos criar dois arquivos, o primeiro é o arquivo `hosts` onde vamos mapear nosso inventário, ou seja, os servidores que precisamos configurar usando o playbook, e o segundo arquivo é o `app.yml` onde estarão contidos todas as tarefas necessárias para implantarmos a aplicação no servidor.

### Arquivo hosts

O arquivo `hosts` possui uma sintaxe muito simples assim como praticamente todas as outras configurações no Ansible usando o padrão de formatação de arquivos `YAML` que usa a tabulação para organizar as informações.

A sintaxe para declarar os servidores no arquivo `hosts` pode ser vista abaixo:

```
[cpf-server]
devops.prod
```

A leitura é simples, entre colchetes identifica um nome para o servidor e abaixo o ip ou domínio do mesmo. Nesse arquivo você pode declarar quantos hosts forem necessários.

### Arquivo app.yml

O arquivo `app.yml` segue a sintaxe de arquivos `YAML` (https://en.wikipedia.org/wiki/YAML), e a única premissa para podermos usar o ansible para configurar nossos servidores é que seja possível acessar via SSH o servidor. No nosso exemplo nós já temos esse acesso, então vamos para os próximos passos.

Vamos começar a declarar as tarefas que são necessárias para a configuração do nosso servidor:

0.  Antes de iniciar a inserção das tasks, precisamos declarar os hosts no playbook:

```
---
- hosts: cpf-server
  remote_user: root

  tasks:
    # as tasks serão declaradas a partir daqui
```

1.  Primeira parte é instalar os pacotes essenciais e básicos para instalação e uso dos serviços, vamos declarar a instalação desses pacotes inserindo as instruções no arquivo `app.yml`:

```
tasks:

  - name: instalar curl, vim, git e build-essential
    apt:
      pkg: ["git", "curl", "vim", "build-essential"]
      state: present
      update_cache: true
      force_apt_get: yes
```

Essa tarefa irá verificar os pacotes git, curl, vim e build-essential e caso algum deles não esteja instalado essa task irá assegurar que esteja.

2.  O próximo passo é declarar a instalação do NodeJS. Para isso vamos adicionar nas linhas seguintes a declaração das tasks para instalação do NodeJS

```
- name: instalar gpg key para o nodejs
  apt_key:
    url: "https://deb.nodesource.com/gpgkey/nodesource.gpg.key"
    state: present

- name: adicionar repositorio do nodejs
  apt_repository:
    repo: "deb https://deb.nodesource.com/node_10.x {{ ansible_distribution_release }} main"
    state: present
    update_cache: yes

- name: instalar nodejs
  apt:
    name: nodejs
    state: present
    force_apt_get: yes
```

Essas tasks na sequência realizam as seguintes atividades, a primeira instala a chave do repositório, depois é inserido na lista de respositórios de pacotes do sistema operacional o repositório do NodeJS, e por último é chamada a task para a instalação do pacote nodejs.

3.  Instalar o gerenciador de processos do NodeJS o PM2. Usando o próprio nodejs que foi instalado vamos instalar o PM2, as instruções seguem abaixo:

```
- name: instalar pm2
  shell: 'npm install pm2 -g'
```

4.  O próximo passo é instalar e configurar o servidor Web usando o NGINX, vamos declarar o NGINX no playbook.

```
- name: instalar nginx
    apt:
      name: nginx
      state: present
      update_cache: true
      force_apt_get: yes
```

Ainda precisamos colocar as configurações da aplicação no servidor web, para isso vamos declarar a cópia do template com as configurações da aplicação para o servidor. O template (arquivo com as configurações) já está na pasta, vamos adicionar a declaração.

```
- name: adicionar arquivo de configuracao no nginx
  template: src=cpf_validator_api.conf dest=/etc/nginx/conf.d/
```

Com essa task o arquivo de configuração será avaliado se tiverem alterações é adicionado novamente caso seja necessário.

5.  Precisamos colocar a aplicação no servidor para poder iniciá-la e disponibilizá-la. Agora vamos adicionar a task para realizar o clone ou atualização do projeto no servidor.

```
- name: atualizacao do projeto no servidor
  git: repo='https://gitlab.com/pigor/cpf_validator_api.git'
       dest=/opt/cpf_validator_api
```

Essa task irá atualizar nosso projeto na pasta de destino usando o git.

6.  Após a atualização do projeto na pasta vamos precisar instalar as dependências do projeto através do `npm install`, vamos adicionar essa tarefa:

```
- name: instalar dependencias
  npm:
    path: /opt/cpf_validator_api
```

7.  Vamos declarar a task para iniciar o processo da aplicação usando o PM2:

```
- name: iniciar processo
  command: chdir=/opt/cpf_validator_api pm2 start --name "cpf_validator_api" app.js
```

8.  Agora só falta reiniciar o nginx, vamos adicionar a task para isso:

```
- name: restart nginx
  service: name=nginx state=restarted
```

9.  Todas as tasks necessárias foram declaradas, agora vamos executar o playbook que irá executar as tasks em sequência e poderemos analisar os problemas para melhorar nosso playbook.

> ansible-playbook -i hosts app.yml

10. Testar se a aplicação ficou disponível acessando `http://devops.prod/api`

11. Para testar o validador tente: `http://devops.prod/api/validator?cpf=<os números de um cpf aqui>`